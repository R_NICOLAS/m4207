const int buttonPin = 5;     // crée un identifiant pour la broche utilisée avec le BP
const int ledPin =  6;      // crée un identifiant pour la broche utilisée avec la LED

// Les variables sont modifiées au cours du programme
int buttonState = 0;         // variable pour mémoriser l'état du bouton

void setup() {
  // configure la broche numérique en SORTIE
  pinMode(ledPin, OUTPUT);      
  // configure la broche numérique en SORTIE
  pinMode(buttonPin, INPUT);    
}

void loop(){
  // lit la valeur de l'état du bouton et la mémorise dans la variable
  buttonState = digitalRead(buttonPin);

  // Teste si le bouton est appuyé
  // c'est à dire si la variable buttonState est à 1
  // NB : ne pas confondre = et == !
  if (buttonState == HIGH) {    
    // allume la LED   
    digitalWrite(ledPin, HIGH);  
  }
  else { // sinon 
    // éteint la LED
    digitalWrite(ledPin, LOW);
  }
}