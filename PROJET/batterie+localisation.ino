#include <LWiFi.h>
#include <LBattery.h>
const int led = 6;  // crée un identifiant pour la broche utilisée avec la LED

char buff[256];

#define WIFI_AP "RT-WIFI-TMP"            // replace your WiFi AP SSID
#define WIFI_PASSWORD "IUTRT97410"  // replace your WiFi AP password
#define WL_MAC_ADDR_LENGTH 6



void setup()
{
  Serial.begin(115200);
  pinMode(led,OUTPUT);
}

void loop()
{
//wifi--------------------------------------------
 LWiFi.begin();   // On active le wifi
 LWiFi.connect("RT-WIFI-TMP", LWiFiLoginInfo(LWIFI_WPA, "IUTRT97410"));
  printWifiStatus();
     delay(1000);
     LWiFi.end();     // on éteins le wifi
     
//batterie----------------------------------------
Serial.println(" ");
 sprintf(buff,"battery level = %d", LBattery.level() );
  Serial.println(buff);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
  if(LBattery.level()<=100){  //si la batterie est inferieur ou égal au nombre indiqué la led s'allume
    digitalWrite(led,HIGH);
  }
  else{   //sinon la led reste eteint
    digitalWrite(led,LOW);  
  }
  
}
void printWifiStatus(){
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());
  
  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());
  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());
  
  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
  

  delay(100);
  uint8_t BSSID[WL_MAC_ADDR_LENGTH] = {0};
  LWiFi.BSSID(BSSID);
  Serial.print("L'adresse MAC en décimal est: ");

  String addMAC;
  for (int i=0; i<WL_MAC_ADDR_LENGTH; i++){      
      addMAC = addMAC + BSSID[i];              
    }
    
    Serial.println(addMAC); 
     if (addMAC == "1624216871206165") {          
          Serial.print("Vous êtes en TD1");
        }
     else if (addMAC == "162421687417285") {          
          Serial.print("Vous êtes en TD3");
        }

     else if (addMAC == "162421687417346") {          
          Serial.print("Vous êtes en Res-Cab");
        }
     else if (addMAC == "162421687417546") {          
          Serial.print("Vous êtes en Sign-Sys");
     }
     else if (addMAC == "162421687417338") {          
          Serial.print("Vous êtes en Proj-Doc");
     }
     else if (addMAC == "162421689092104") {          
          Serial.print("Vous êtes en Admin");
     }   
        
        else {
          Serial.print("Erreur");
          }

  
}