#include <LWiFi.h>



#define WIFI_AP "RT-WIFI-TMP"            // replace your WiFi AP SSID
#define WIFI_PASSWORD "IUTRT97410"  // replace your WiFi AP password
#define WL_MAC_ADDR_LENGTH 6

/*
Liste addresse mac en hexa et en decimal fonction des salles :

RT-WF-TMP-ADM: A2:2A:A8:5A:5C:68 / 162421689092104
RT-WF-TMP-PJD: A2:2A:A8:4A:AD:26 / 162421687417338
RT-WF-TMP-RC:  A2:2A:A8:4A:AD:2E / 162421687417346
RT-WF-TMP-SS:  A2:2A:A8:4A:AF:2E / 162421687417546
RT-WF-TMP-TD1: A2:2A:A8:47:CE:A5 / 1624216871206165
RT-WF-TMP-TD3: A2:2A:A8:4A:AC:55 / 162421687417285
*/


void setup()
{
  Serial.begin(115200);

}

void loop()
{
 LWiFi.begin();   // On active le wifi
 LWiFi.connect("RT-WIFI-TMP", LWiFiLoginInfo(LWIFI_WPA, "IUTRT97410"));
  printWifiStatus();
     delay(1000);
     LWiFi.end();     // on éteins le wifi
}

void printWifiStatus(){
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());
  
  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());
  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());
  
  // puissance du signal:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
  

  delay(100);
  uint8_t BSSID[WL_MAC_ADDR_LENGTH] = {0};
  LWiFi.BSSID(BSSID);
  Serial.print("L'adresse MAC en décimal est: ");

  String addMAC;
  for (int i=0; i<WL_MAC_ADDR_LENGTH; i++){      
      addMAC = addMAC + BSSID[i];              
    }
    
    Serial.println(addMAC); 
     if (addMAC == "1624216871206165") {          
          Serial.print("Vous êtes en TD1");
        }
     else if (addMAC == "162421687417285") {          
          Serial.print("Vous êtes en TD3");
        }

     else if (addMAC == "162421687417346") {          
          Serial.print("Vous êtes en Reseau Cablage");
        }
     else if (addMAC == "162421687417546") {          
          Serial.print("Vous êtes en Sig-Systeme");
     }
     else if (addMAC == "162421687417338") {          
          Serial.print("Vous êtes en Proj-Doc");
     }
     else if (addMAC == "162421689092104") {          
          Serial.print("Vous êtes en Administration");
     }   
        
        else {
          Serial.print("Erreur");
          }

  
}
