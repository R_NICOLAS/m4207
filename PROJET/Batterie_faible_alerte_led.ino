#include <LBattery.h>
  // crée un identifiant pour la broche utilisée avec la LED
const int led = 6;  

char buff[256];

void setup() {
  // configure la broche numérique en SORTIE
  pinMode(led,OUTPUT);  
  Serial.begin(115200);
}


void loop() {
  
  sprintf(buff,"battery level = %d", LBattery.level() );
  Serial.println(buff);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
  
  //si la batterie est inferieur ou égal au nombre indiqué la led s'allume
  if(LBattery.level()<=66){  
    digitalWrite(led,HIGH);
  }
  
  //sinon la led reste eteint
  else{   
    digitalWrite(led,LOW);  
  }
  
   delay(1000); 
}